import "reflect-metadata";

@printMetadata
class Plane {
  color: string = "red";

  @markFunction("Eita")
  fly(): void {
    console.log("vrrrr");
  }
}

function markFunction(secretInfo: string) {
  return function (target: Plane, key: string) {
    Reflect.defineMetadata("secret", secretInfo, target, key);
  };
}

function printMetadata(target: typeof Plane) {
  for (let key in target.prototype) {
    const secret = Reflect.getMetadata("secret", target.prototype, key);
  }
}

const secret = Reflect.getMetadata("secret", Plane.prototype, "fly");

console.log(secret);

// const plane = {
//   color: "red",
// };

// Reflect.defineMetadata("note", "hi there", plane, "color");

// const otherNote = Reflect.getMetadata("note", plane, "color");

// Reflect.defineMetadata("note", "yaw yo", plane);
// Reflect.defineMetadata("height", 10, plane);

// // console.log(plane);

// const note = Reflect.getMetadata("note", plane);
// const height = Reflect.getMetadata("height", plane);

// console.log(note);
// console.log(height);
// console.log(otherNote);
